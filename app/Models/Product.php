<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model{
    use HasFactory;
    // public function getCategory() {
    //     return $category = Category::find($this->category_id);
    //     dd($category);
    // }

    public function category() {
        return $this->belongsTo(Product::class);
    }

    public function getPriceForCount() {
        if (!is_null($this->pivot->count)) {
            return $this->price * $this->pivot->count;
        } else {
            return $this->price;
        }
    }
}
