<div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <img src="http://internet-shop.tmweb.ru/storage/products/iphone_x.jpg" alt="iPhone X 64GB">
        <div class="caption">
            @isset($product)
            <h3>{{ $product->name }}</h3>
            <p>{{ $product->price }} ₽</p>
            @endisset
            <p>
                <form action="{{ route('basket-add', $product) }}" method="POST">
                    <button type="submit" class="btn btn-primary" role="button">В корзину</button>
                    <a href="{{ route('product', [$product->category->code, $product->code]) }}" class="btn btn-default" role="button">Подробнее</a>
                    <input type="hidden" name="_token" value="eSou0vetJOervkSpJTMMwQkQGCiXDASuWp6cYvzX"> 
                    @csrf
                </form>
            </p>
        </div>
    </div>
</div>