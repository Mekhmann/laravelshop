@extends('layouts.master') 
@section('title', 'Товар') 
@section('content')
    <h1>iPhone X64GB</h1>
    <h2>{{ $product }}</h2>
    <p>Цена: <b>71998</b></p>
    <p>Отличный продвинутый телефон с памятью на 64 gb</p>
    <a class="btn btn-success" href="#">Добавить в корзину</a>
@endsection
